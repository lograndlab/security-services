﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security;

[assembly: OwinStartup(typeof(Logrand.LOS.Security.WS.Startup))]

namespace Logrand.LOS.Security.WS
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            config.MapHttpAttributeRoutes();
            config.Filters.Add(new Auth.AuthSession());

            ConfigureOAuth(app);

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            app.UseWebApi(config);
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            var issuer = "http://localhost/security";
            var audience = "cc73800ebd6544d793b88a2aec59ca41";
            var secret = TextEncodings.Base64Url.Decode("JsGe3UcjFwcCgkwOKYp_QE4UIW_1klbC0x7SRSo4Jr9bhDk2Dqj95750uRzR12xx9yv-3AGvN2B3_dgaPK4rKg");

            // Api controllers with an [Authorize] attribute will be validated with JWT
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] { audience },
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new SymmetricKeyIssuerSecurityTokenProvider(issuer, secret)
                    }
                });

        }
    }
}
