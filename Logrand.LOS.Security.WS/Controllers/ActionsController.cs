﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Logrand.LOS.Security.WS.Controllers
{
    [Authorize]
    [RoutePrefix("api/actions")]
    public class ActionsController : ApiController
    {
        [HttpGet]
        [Route("")]
        public HttpResponseMessage getActions()
        {
            HttpResponseMessage message = new HttpResponseMessage();

            message = Request.CreateResponse(HttpStatusCode.OK, Models.Action.getActions());

            return message;
        }

        [HttpGet]
        [Route("{ActionID}")]
        public HttpResponseMessage getAction(int ActionID)
        {
            HttpResponseMessage message = new HttpResponseMessage();

            Models.Action result = Models.Action.getAction(ActionID);

            if (result != null)
                message = Request.CreateResponse(HttpStatusCode.OK, result);
            else
            {
                message = Request.CreateResponse(HttpStatusCode.NotFound, new Response.BAD()
                {
                    ErrorMessage = "Application no se encontró",
                    TecnicalMessage = "No se encotró en base de datos"
                });
            }

            return message;
        }
    }
}
