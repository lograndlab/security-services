﻿using Logrand.LOS.Security.WS.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace Logrand.LOS.Security.WS.Controllers
{
    [Authorize]
    [RoutePrefix("api/applications")]
    public class ApplicationsController : ApiController
    {

        [HttpGet]
        [Route("")]
        public HttpResponseMessage getApplications()
        {
            HttpResponseMessage message = new HttpResponseMessage();

            message = Request.CreateResponse(HttpStatusCode.OK, Models.Application.getApplications(null));

            return message;
        }

        [HttpGet]
        [Route("{ApplicationID}")]
        public HttpResponseMessage getApplication(int ApplicationID)
        {
            HttpResponseMessage message = new HttpResponseMessage();

            Models.Application result = Models.Application.getApplication(ApplicationID);

            if (result != null)
                message = Request.CreateResponse(HttpStatusCode.OK, result);
            else
            {
                message = Request.CreateResponse(HttpStatusCode.NotFound, new Response.BAD()
                {
                    ErrorMessage = "Application no se encontró",
                    TecnicalMessage = "No se encotró en base de datos"
                });
            }

            return message;
        }

        [HttpPut]
        [Route("")]
        public HttpResponseMessage create(Models.Application application)
        {
            HttpResponseMessage message = new HttpResponseMessage();

            try
            {
                Models.Application result = Models.Application.createApplication(application);
                message = Request.CreateResponse(HttpStatusCode.Created, result);
            }
            catch (Exception e)
            {
                message = Request.CreateResponse(HttpStatusCode.BadRequest, new Response.BAD()
                {
                    ErrorMessage = "No se pudo agregar la aplicación",
                    TecnicalMessage = e.InnerException.Message
                });
            }

            return message;
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage update(Models.Application application)
        {
            HttpResponseMessage message = new HttpResponseMessage();

            try
            {
                Models.Application result = Models.Application.updateApplication(application);
                message = Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                message = Request.CreateResponse(HttpStatusCode.BadRequest, new Response.BAD()
                {
                    ErrorMessage = "No se pudo agregar la aplicación",
                    TecnicalMessage = e.InnerException.Message
                });
            }

            return message;
        }

        [HttpGet]
        [Route("{ApplicationID}/status/{status}")]
        public HttpResponseMessage changeStatus(int ApplicationID, Boolean status)
        {
            HttpResponseMessage message = new HttpResponseMessage();

            if (ApplicationID == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response.BAD()
                {
                    ErrorMessage = "Faltan parámetros",
                    TecnicalMessage = "ApplicationID está vacío"
                });
            }

            try
            {
                Models.Application result = Models.Application.changeStatus(ApplicationID, status);
                message = Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                message = Request.CreateResponse(HttpStatusCode.BadRequest, new Response.BAD()
                {
                    ErrorMessage = "No se pudo agregar la aplicación",
                    TecnicalMessage = e.InnerException.Message
                });
            }

            return message;
        }
    }
}
