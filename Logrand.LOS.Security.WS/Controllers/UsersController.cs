﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Logrand.LOS.Security.WS.Controllers
{
    [Authorize]
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        [HttpGet]
        [Route("")]
        public HttpResponseMessage getUsers()
        {
            HttpResponseMessage message = new HttpResponseMessage();

            message = Request.CreateResponse(HttpStatusCode.OK, Models.User.getUsers());

            return message;
        }


        [HttpGet]
        [Route("{UserID}")]
        public HttpResponseMessage getUser(int UserID)
        {
            HttpResponseMessage message = new HttpResponseMessage();

            Models.User result = Models.User.getUser(UserID, true);

            if (result != null)
                message = Request.CreateResponse(HttpStatusCode.OK, result);
            else
            {
                message = Request.CreateResponse(HttpStatusCode.NotFound, new Response.BAD()
                {
                    ErrorMessage = "User no se encontró",
                    TecnicalMessage = "No se encotró en base de datos"
                });
            }

            return message;
        }
    }
}
