﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Logrand.LOS.Security.WS.Controllers
{
    [Authorize]
    [RoutePrefix("api/dashboard")]
    public class DashboardController : ApiController
    {
        [HttpGet]
        [Route("")]
        public HttpResponseMessage getActions()
        {
            HttpResponseMessage message = new HttpResponseMessage();

            message = Request.CreateResponse(HttpStatusCode.OK, Models.Dashboard.getDashboard());

            return message;
        }
    }
}
