﻿using Logrand.LOS.Security.WS.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace Logrand.LOS.Security.WS.Controllers
{
    [Authorize]
    [RoutePrefix("api/sessions")]
    public class SessionsController : ApiController
    {

        [HttpGet]
        [Route("")]
        public HttpResponseMessage getSessions()
        {
            HttpResponseMessage message = new HttpResponseMessage();

            message = Request.CreateResponse(HttpStatusCode.OK, Models.Session.getSessions(null, true, true));

            return message;
        }

        [HttpGet]
        [Route("active")]
        public HttpResponseMessage getActiveSessions()
        {
            HttpResponseMessage message = new HttpResponseMessage();

            message = Request.CreateResponse(HttpStatusCode.OK, Models.Session.getSessions(x=>x.Active == true || x.LogoutAt == null, true, true));

            return message;
        }

        [HttpGet]
        [Route("{SessionID}")]
        public HttpResponseMessage getSession(int SessionID)
        {
            HttpResponseMessage message = new HttpResponseMessage();

            Models.Session result = Models.Session.getSession(SessionID, true, true);

            if (result != null)
                message = Request.CreateResponse(HttpStatusCode.OK, result);
            else
            {
                message = Request.CreateResponse(HttpStatusCode.NotFound, new Response.BAD()
                {
                    ErrorMessage = "Session no se encontró",
                    TecnicalMessage = "No se encotró en base de datos"
                });
            }

            return message;
        }

        [HttpGet]
        [Route("{SessionID}/status/{status}")]
        public HttpResponseMessage changeStatus(int SessionID, Boolean status)
        {
            HttpResponseMessage message = new HttpResponseMessage();

            if (SessionID == 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response.BAD()
                {
                    ErrorMessage = "Faltan parámetros",
                    TecnicalMessage = "SessionID está vacío"
                });
            }

            try
            {
                var UserID = IdentitySession.getUserIDFormSession((ClaimsIdentity)User.Identity);

                Models.Session result = Models.Session.changeStatus(SessionID, UserID, status);
                message = Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                message = Request.CreateResponse(HttpStatusCode.BadRequest, new Response.BAD()
                {
                    ErrorMessage = "No se pudo agregar la aplicación",
                    TecnicalMessage = e.InnerException.Message
                });
            }

            return message;
        }
    }
}
