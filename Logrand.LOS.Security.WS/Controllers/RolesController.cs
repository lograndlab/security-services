﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Logrand.LOS.Security.WS.Controllers
{
    [Authorize]
    [RoutePrefix("api/roles")]
    public class RolesController : ApiController
    {
        [HttpGet]
        [Route("")]
        public HttpResponseMessage getRoles()
        {
            HttpResponseMessage message = new HttpResponseMessage();

            message = Request.CreateResponse(HttpStatusCode.OK, Models.Role.getRoles());

            return message;
        }


        [HttpGet]
        [Route("{RoleID}")]
        public HttpResponseMessage getRole(int RoleID)
        {
            HttpResponseMessage message = new HttpResponseMessage();

            Models.Role result = Models.Role.getRole(RoleID);

            if (result != null)
                message = Request.CreateResponse(HttpStatusCode.OK, result);
            else
            {
                message = Request.CreateResponse(HttpStatusCode.NotFound, new Response.BAD()
                {
                    ErrorMessage = "User no se encontró",
                    TecnicalMessage = "No se encotró en base de datos"
                });
            }

            return message;
        }
    }
}
