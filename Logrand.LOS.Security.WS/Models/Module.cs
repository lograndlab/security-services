﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Logrand.LOS.Security.WS.Models
{
    public class Module
    {
        //public int ID { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        //public int ApplicationID { get; set; }
        //public Nullable<DateTime> CreatedAt { get; set; }
        //public Nullable<DateTime> ModifiedAt { get; set; }
        //public Nullable<bool> Active { get; set; }

        //public List<Action> Actions { get; set; }
        //public Application Application { get; set; }

        //public static List<Module> getModules(Expression<Func<Security.Models.Module, bool>> expression = null, Boolean withApplication = false, Boolean withActions = false)
        //{
        //    Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

        //    List<Security.Models.Module> results;
        //    if (expression == null)
        //        results = db.Modules.ToList();
        //    else
        //        results = db.Modules.Where(expression).ToList();

        //    List<Module> models = new List<Module>();

        //    foreach (Security.Models.Module result in results)
        //    {
        //        Module model = new Module();
        //        model.ID = result.ID;
        //        model.Name = result.Name;
        //        model.Description = result.Description;
        //        model.ApplicationID = result.ApplicationID;
        //        model.CreatedAt = result.CreatedAt;
        //        model.ModifiedAt = result.ModifiedAt;
        //        model.Active = result.Active;

        //        model.Application = withApplication ? Application.getApplication(result.ApplicationID) : null;
        //        model.Actions = withActions ? Action.getActions(x => x.ModuleID == result.ID) : null;
        //        models.Add(model);
        //    }

        //    return models;
        //}

        //public static Module getModule(int ModuleID, Boolean withApplication = false, Boolean withActions = false)
        //{
        //    Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

        //    var result = db.Modules.Where(x => x.ID == ModuleID).FirstOrDefault();

        //    if (result == null)
        //        return null;

        //    return new Module()
        //    {
        //        ID = result.ID,
        //        Name = result.Name,
        //        Description = result.Description,
        //        ApplicationID = result.ApplicationID,
        //        CreatedAt = result.CreatedAt,
        //        ModifiedAt = result.ModifiedAt,
        //        Active = result.Active,

        //        Application = withApplication ? Application.getApplication(result.ApplicationID) : null,
        //        Actions = withActions ? Action.getActions(x => x.ModuleID == result.ID) : null
        //    };
        //}
    }
}