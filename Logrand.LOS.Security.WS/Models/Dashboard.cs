﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Security.WS.Models
{
    public class Dashboard
    {
        public int TotalUsers { get; set; }
        public int ActiveUsers { get; set; }
        public int Logins { get; set; }
        public int NewSignups { get; set; }

        public List<Session> ListLatestLogins { get; set; }
        public List<User> ListNewSignups { get; set; }

        public static Dashboard getDashboard()
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();
            Dashboard model = new Dashboard();

            Security.Models.GET_DASHBOARD_INFO_Result result = db.GET_DASHBOARD_INFO().FirstOrDefault();

            if(result != null)
            {
                model.TotalUsers = result.TotalUsers == null ? 0 : (int)result.TotalUsers;
                model.ActiveUsers = result.ActiveUsers == null ? 0 : (int)result.ActiveUsers;
                model.Logins = result.Logins == null ? 0 : (int)result.Logins;
                model.NewSignups = result.NewSignups == null ? 0 : (int)result.NewSignups;
            }

            return model;
        }
    }
}