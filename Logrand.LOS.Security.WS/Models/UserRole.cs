﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Security.WS.Models
{
    public class UserRole
    {
        public int ID { get; set; }
        public int RoleID { get; set; }
        public int UserID { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }

        public List<Role> Roles { get; set; }
        public List<User> Users { get; set; }

        public static List<User> getUsersByRole(int RoleID)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            List<Security.Models.SEC_ROLE_USER> results = db.SEC_ROLE_USER.Where(x => x.RoleId == RoleID).ToList();

            List<User> models = new List<User>();
            foreach (Security.Models.SEC_ROLE_USER result in results)
            {
                User model = new User();

                models.Add(model);
            }

            return models;
        }
    }
}