﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Logrand.LOS.Security.WS.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserPass { get; set; }
        public string UserSalt { get; set; }
        public Nullable<bool> UserActive { get; set; }
        public Nullable<bool> UserBlocked { get; set; }
        public Nullable<byte> UserAttempts { get; set; }
        public Nullable<bool> UserCanChangePass { get; set; }
        public Nullable<System.DateTime> UserLastAccess { get; set; }
        public Nullable<System.DateTime> UserLastChange { get; set; }
        public int UserLastChangeUserId { get; set; }
        public Nullable<int> RoleId { get; set; }
        public Nullable<int> ContactId { get; set; }
        public Nullable<int> SalaId { get; set; }
        public string UserIpAccess { get; set; }
        public string PayrollNumber { get; set; }
        public System.DateTime UserCreatedDate { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> UserCreatedBy { get; set; }
        public string userDomain { get; set; }
        public Nullable<System.DateTime> LastChange { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }

        public List<Application> Applications { get; set; }
        public List<Session> Sessions { get; set; }

        public static List<User> getUsers(Expression<Func<Security.Models.SEC_USER, bool>> expression = null, Boolean withSessions = false)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            List<Security.Models.SEC_USER> results;
            if (expression == null)
                results = db.SEC_USER.ToList();
            else
                results = db.SEC_USER.Where(expression).ToList();


            List<User> models = new List<User>();

            foreach (Security.Models.SEC_USER result in results)
            {
                User model = new User();

                model.UserId = result.UserId;
                model.UserName = result.UserName;
                //model.UserPass = result.UserPass;
                //model.UserSalt = result.UserSalt;
                model.UserActive = result.UserActive;
                model.UserBlocked = result.UserBlocked;
                model.UserAttempts = result.UserAttempts;
                model.UserCanChangePass = result.UserCanChangePass;
                model.UserLastAccess = result.UserLastAccess;
                model.UserLastChange = result.UserLastChange;
                model.UserLastChangeUserId = result.UserLastChangeUserId;
                model.RoleId = result.RoleId;
                model.ContactId = result.ContactId;
                model.SalaId = result.SalaId;
                model.UserIpAccess = result.UserIpAccess;
                model.PayrollNumber = result.PayrollNumber;
                model.UserCreatedDate = result.UserCreatedDate;
                model.EmployeeId = result.EmployeeId;
                model.UserCreatedBy = result.UserCreatedBy;
                model.userDomain = result.userDomain;
                model.LastChange = result.LastChange;
                model.SyncDate = result.SyncDate;

                //Models
                model.Sessions = withSessions ? Session.getSessions(x => x.UserID == result.UserId) : null;
                models.Add(model);
            }

            return models;
        }

        public static User getUser(int UserID, Boolean withSessions = false)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            var result = db.SEC_USER.Where(x => x.UserId == UserID).FirstOrDefault();

            if (result == null)
                return null;

            return new User()
            {
                UserId = result.UserId,
                //UserName = result.UserName,
                //UserPass = result.UserPass,
                UserSalt = result.UserSalt,
                UserActive = result.UserActive,
                UserBlocked = result.UserBlocked,
                UserAttempts = result.UserAttempts,
                UserCanChangePass = result.UserCanChangePass,
                UserLastAccess = result.UserLastAccess,
                UserLastChange = result.UserLastChange,
                UserLastChangeUserId = result.UserLastChangeUserId,
                RoleId = result.RoleId,
                ContactId = result.ContactId,
                SalaId = result.SalaId,
                UserIpAccess = result.UserIpAccess,
                PayrollNumber = result.PayrollNumber,
                UserCreatedDate = result.UserCreatedDate,
                EmployeeId = result.EmployeeId,
                UserCreatedBy = result.UserCreatedBy,
                userDomain = result.userDomain,
                LastChange = result.LastChange,
                SyncDate = result.SyncDate,
                //Models
                Sessions = withSessions ? Session.getSessions(x => x.UserID == result.UserId) : null,
            };
        }
    }
}