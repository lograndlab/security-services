﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Logrand.LOS.Security.WS.Models
{
    public class Session
    {
        public long ID { get; set; }
        public int UserID { get; set; }
        public int ApplicationID { get; set; }
        public string AccessToken { get; set; }
        public string IpAddress { get; set; }
        public Nullable<int> LogoutBy { get; set; }
        public Nullable<System.DateTime> LogoutAt { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<System.DateTime> ExpiredAt { get; set; }
        public Nullable<bool> Active { get; set; }

        public User User { get; set; }
        public Application Application { get; set; }

        public static List<Session> getSessions(Expression<Func<Security.Models.SEC_SESSION, bool>> expression = null, Boolean withUser = false, Boolean withApplication = false)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            List<Security.Models.SEC_SESSION> results;
            if (expression == null)
                results = db.SEC_SESSION.ToList();
            else
                results = db.SEC_SESSION.Where(expression).ToList();

            List<Session> models = new List<Session>();

            foreach (Security.Models.SEC_SESSION result in results)
            {
                Session model = new Session();

                model.ID = result.ID;
                model.UserID = result.UserID;
                model.ApplicationID = result.ApplicationID;
                model.AccessToken = result.AccessToken;
                model.IpAddress = result.IpAddress;
                model.LogoutBy = result.LogoutBy;
                model.LogoutAt = result.LogoutAt;
                model.CreatedAt = result.CreatedAt;
                model.ExpiredAt = result.ExpiredAt;

                if (result.LogoutAt == null)
                    model.Active = true;

                model.User = withUser ? User.getUser(result.UserID) : null;
                model.Application = withApplication ? Application.getApplication(result.ApplicationID) : null;

                models.Add(model);
            }

            return models;
        }

        public static Session getSession(int SessionID, Boolean withUser = false, Boolean withApplication = false)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            db.Configuration.ProxyCreationEnabled = false;

            var result = db.SEC_SESSION.Where(x => x.ID == SessionID).FirstOrDefault();

            if (result == null)
                return null;

            return new Session()
            {
                ID = result.ID,
                UserID = result.UserID,
                ApplicationID = result.ApplicationID,
                AccessToken = result.AccessToken,
                IpAddress = result.IpAddress,
                LogoutBy = result.LogoutBy,
                LogoutAt = result.LogoutAt,
                CreatedAt = result.CreatedAt,
                ExpiredAt = result.ExpiredAt,
                Active = result.Active,

                User = withUser ? User.getUser(result.UserID) : null,
                Application = withApplication ? Application.getApplication(result.ApplicationID) : null
            };
        }

        public static Session changeStatus(int Session, int UserID, Boolean status)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();
            Security.Models.SEC_SESSION result = db.SEC_SESSION.Where(x => x.ID == Session).FirstOrDefault();

            result.LogoutAt = DateTime.Now;
            result.Active = status;

            db.Entry(result).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            Session model = new Session();
            model.ID = result.ID;
            model.UserID = result.UserID;
            model.ApplicationID = result.ApplicationID;
            model.AccessToken = result.AccessToken;
            model.IpAddress = result.IpAddress;
            model.LogoutBy = result.LogoutBy;
            model.LogoutAt = result.LogoutAt;
            model.CreatedAt = result.CreatedAt;
            model.ExpiredAt = result.ExpiredAt;

            return model;
        }
    }
}