﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Security.WS.Models
{
    public class RoleAction
    {
        public int ID { get; set; }
        public int RoleID { get; set; }
        public int ActionID { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }

        public List<Role> Roles { get; set; }
        public List<Action> Actions { get; set; }

        public static List<Action> getActionsByRole(int RoleID)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            List<Security.Models.SEC_PERMISSION_ROLE> results = db.SEC_PERMISSION_ROLE.Where(x => x.RoleId == RoleID).ToList();

            List<Action> models = new List<Action>();
            foreach (Security.Models.SEC_PERMISSION_ROLE result in results)
            {
                Security.Models.SEC_PERMISSION permission = db.SEC_PERMISSION.Where(x => x.PermissionId == result.PermissionId).FirstOrDefault();
                Action model = new Action();

                model.PermissionId = permission.PermissionId;
                model.PermisionName = permission.PermisionName;
                model.PermissionActive = permission.PermissionActive;
                model.PermissionDescription = permission.PermissionDescription;
                model.PermissionLastChange = permission.PermissionLastChange;
                model.PermissionLastChangeUserId = permission.PermissionLastChangeUserId;
                model.OpcMenuId = permission.OpcMenuId;
                model.LastChange = permission.LastChange;
                model.SyncDate = permission.SyncDate;

                models.Add(model);
            }

            return models;
        }
    }
}