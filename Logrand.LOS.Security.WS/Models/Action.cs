﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Logrand.LOS.Security.WS.Models
{
    public class Action
    {
        public int PermissionId { get; set; }
        public string PermisionName { get; set; }
        public bool PermissionActive { get; set; }
        public string PermissionDescription { get; set; }
        public System.DateTime PermissionLastChange { get; set; }
        public int PermissionLastChangeUserId { get; set; }
        public int OpcMenuId { get; set; }
        public Nullable<System.DateTime> LastChange { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }

        public static List<Action> getActions(Expression<Func<Security.Models.SEC_PERMISSION, bool>> expression = null)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            List<Security.Models.SEC_PERMISSION> results;
            if (expression == null)
                results = db.SEC_PERMISSION.ToList();
            else
                results = db.SEC_PERMISSION.Where(expression).ToList();

            List<Action> models = new List<Action>();

            foreach (Security.Models.SEC_PERMISSION result in results)
            {
                Action model = new Action();
                model.PermissionId = result.PermissionId;
                model.PermisionName = result.PermisionName;
                model.PermissionActive = result.PermissionActive;
                model.PermissionDescription = result.PermissionDescription;
                model.PermissionLastChange = result.PermissionLastChange;
                model.PermissionLastChangeUserId = result.PermissionLastChangeUserId;
                model.OpcMenuId = result.OpcMenuId;
                model.LastChange = result.LastChange;
                model.SyncDate = result.SyncDate;

                models.Add(model);
            }

            return models;
        }

        public static Action getAction(int ActionID)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            var result = db.SEC_PERMISSION.Where(x => x.PermissionId == ActionID).FirstOrDefault();

            if (result == null)
                return null;

            return new Action()
            {
                PermissionId = result.PermissionId,
                PermisionName = result.PermisionName,
                PermissionActive = result.PermissionActive,
                PermissionDescription = result.PermissionDescription,
                PermissionLastChange = result.PermissionLastChange,
                PermissionLastChangeUserId = result.PermissionLastChangeUserId,
                OpcMenuId = result.OpcMenuId,
                LastChange = result.LastChange,
                SyncDate = result.SyncDate
            };
        }
    }
}