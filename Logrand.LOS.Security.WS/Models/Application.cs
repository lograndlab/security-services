﻿using Microsoft.Owin.Security.DataHandler.Encoder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Web;

namespace Logrand.LOS.Security.WS.Models
{
    public class Application
    {
        public int AppId { get; set; }
        public string AppName { get; set; }
        public string AppImage { get; set; }
        public string AppURL { get; set; }
        public bool AppActive { get; set; }
        public string Domain { get; set; }
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public Nullable<System.DateTime> LastChange { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }

        public User User { get; set; }
        public ApplicationTypes ApplicationType { get; set; }

        public static List<Application> getApplications(Expression<Func<Security.Models.SEC_APPLICATION,bool>> expression = null)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            List<Security.Models.SEC_APPLICATION> results;
            if (expression == null)
                results = db.SEC_APPLICATION.ToList();
            else
                results = db.SEC_APPLICATION.Where(expression).ToList();

            List<Application> models = new List<Application>();

            foreach (Security.Models.SEC_APPLICATION result in results)
            {
                Application model = new Application();

                model.AppId = result.AppId;
                model.AppName = result.AppName;
                model.AppImage = result.AppImage;
                model.AppURL = result.AppURL;
                model.AppActive = result.AppActive;
                model.Domain = result.Domain;
                model.ClientID = result.ClientID;
                model.ClientSecret = result.ClientSecret;
                model.LastChange = result.LastChange;
                model.SyncDate = result.SyncDate;

                models.Add(model);
            }

            return models;
        }

        public static Application getApplication(int ApplicationID, Boolean withUser = false)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            var result = db.SEC_APPLICATION.Where(x => x.AppId == ApplicationID).FirstOrDefault();

            if (result == null)
                return null;

            return new Application()
            {
                AppId = result.AppId,
                AppName = result.AppName,
                AppImage = result.AppImage,
                AppURL = result.AppURL,
                AppActive = result.AppActive,
                Domain = result.Domain,
                ClientID = result.ClientID,
                ClientSecret = result.ClientSecret,
                LastChange = result.LastChange,
                SyncDate = result.SyncDate
            };
        }

        public static Application createApplication(Application application)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();
            Security.Models.SEC_APPLICATION model = new Security.Models.SEC_APPLICATION();

            model.AppName = application.AppName;
            model.AppImage = application.AppImage;
            model.AppURL = application.AppURL;
            model.AppActive = true;
            model.Domain = application.Domain;
            model.LastChange = application.LastChange;
            model.SyncDate = application.SyncDate;

            model.ClientID = Guid.NewGuid().ToString("N");
            var key = new byte[64];
            RNGCryptoServiceProvider.Create().GetBytes(key);
            model.ClientSecret = TextEncodings.Base64Url.Encode(key);

            db.SEC_APPLICATION.Add(model);
            db.SaveChanges();

            application.AppId = model.AppId;
            application.AppName = model.AppName;
            application.AppImage = model.AppImage;
            application.AppURL = model.AppURL;
            application.AppActive = model.AppActive;
            application.Domain = model.Domain;
            application.ClientID = model.ClientID;
            application.ClientSecret = model.ClientSecret;
            application.LastChange = model.LastChange;
            application.SyncDate = model.SyncDate;

            return application;
        }

        public static Application updateApplication(Application application)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();
            Security.Models.SEC_APPLICATION model = db.SEC_APPLICATION.Where(x => x.AppId == application.AppId).FirstOrDefault();

            model.AppName = application.AppName;
            model.AppImage = application.AppImage;
            model.AppURL = application.AppURL;
            model.AppActive = application.AppActive;
            model.Domain = application.Domain;
            model.LastChange = DateTime.Now;
            model.SyncDate = application.SyncDate;

            db.Entry(model).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            application.AppId = model.AppId;
            application.AppName = model.AppName;
            application.AppImage = model.AppImage;
            application.AppURL = model.AppURL;
            application.AppActive = model.AppActive;
            application.Domain = model.Domain;
            application.ClientID = model.ClientID;
            application.ClientSecret = model.ClientSecret;
            application.LastChange = model.LastChange;
            application.SyncDate = model.SyncDate;

            return application;
        }

        public static Application changeStatus(int ApplicationID, Boolean status)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();
            Security.Models.SEC_APPLICATION model = db.SEC_APPLICATION.Where(x => x.AppId == ApplicationID).FirstOrDefault();


            model.LastChange = DateTime.Now;
            model.AppActive = status;

            db.Entry(model).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            Application application = new Application();
            application.AppId = model.AppId;
            application.AppName = model.AppName;
            application.AppImage = model.AppImage;
            application.AppURL = model.AppURL;
            application.AppActive = model.AppActive;
            application.Domain = model.Domain;
            application.ClientID = model.ClientID;
            application.ClientSecret = model.ClientSecret;
            application.LastChange = model.LastChange;
            application.SyncDate = model.SyncDate;

            return application;
        }
    }
}