﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Logrand.LOS.Security.WS.Models
{
    public class Role
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public Nullable<int> RoleParentId { get; set; }
        public bool RoleActive { get; set; }
        public System.DateTime RoleLastChange { get; set; }
        public Nullable<int> RoleLastChangeUserID { get; set; }
        public bool ISSYSTEM { get; set; }
        public Nullable<System.DateTime> LastChange { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }

        public List<User> Users { get; set; }
        public List<Action> Actions { get; set; }

        public static List<Role> getRoles(Expression<Func<Security.Models.SEC_ROLE, bool>> expression = null)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            List<Security.Models.SEC_ROLE> results;
            if (expression == null)
                results = db.SEC_ROLE.ToList();
            else
                results = db.SEC_ROLE.Where(expression).ToList();

            List<Role> models = new List<Role>();

            foreach (Security.Models.SEC_ROLE result in results)
            {
                Role model = new Role();

                model.RoleId = result.RoleId;
                model.RoleName = result.RoleName;
                model.RoleDescription = result.RoleDescription;
                model.RoleParentId = result.RoleParentId;
                model.RoleActive = result.RoleActive;
                model.RoleLastChange = result.RoleLastChange;
                model.RoleLastChangeUserID = result.RoleLastChangeUserID;
                model.ISSYSTEM = result.ISSYSTEM;
                model.LastChange = result.LastChange;
                model.SyncDate = result.SyncDate;

                models.Add(model);
            }

            return models;
        }

        public static Role getRole(int ApplicationID)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            var result = db.SEC_ROLE.Where(x => x.RoleId == ApplicationID).FirstOrDefault();

            if (result == null)
                return null;

            return new Role()
            {
                RoleId = result.RoleId,
                RoleName = result.RoleName,
                RoleDescription = result.RoleDescription,
                RoleParentId = result.RoleParentId,
                RoleActive = result.RoleActive,
                RoleLastChange = result.RoleLastChange,
                RoleLastChangeUserID = result.RoleLastChangeUserID,
                ISSYSTEM = result.ISSYSTEM,
                LastChange = result.LastChange,
                SyncDate = result.SyncDate
            };
        }
    }
}