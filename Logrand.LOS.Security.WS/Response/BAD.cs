﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Security.WS.Response
{
    public class BAD
    {
        public string ErrorMessage { get; set; }
        public string TecnicalMessage { get; set; }
    }
}