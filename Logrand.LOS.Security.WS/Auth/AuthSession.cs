﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Logrand.LOS.Security.WS.Auth
{
    public class AuthSession: AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();
            if (actionContext.Request.Headers.Authorization == null)
                return false;
            //Sacar el token enviando en petición
            string token = actionContext.Request.Headers.Authorization.Parameter;
            //Buscar la session del token
            var session = db.SEC_SESSION.Where(x => x.AccessToken == token).FirstOrDefault();

            /** Negar el acceso si estas condiciones se cumplen
             * -- La session no existe.
             * -- Haya hecho Logout (O un administrador haya hecho logout).
             * -- La session no esta activa.
             **/
            if (session == null || session.LogoutAt != null || session.LogoutAt != null || session.Active == false)
                return false;

            return base.IsAuthorized(actionContext);
        }
    }
}