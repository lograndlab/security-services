﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace Logrand.LOS.Security.WS.Auth
{
    public class IdentitySession
    {
        public static int getUserIDFormSession(ClaimsIdentity identity)
        {
            return int.Parse(identity.Claims.Where(x => x.Type == "ID").FirstOrDefault().Value);
        }
    }
}