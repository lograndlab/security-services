﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Security.Auth.Models
{
    public class Claim
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}