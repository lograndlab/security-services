﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Security.Auth.Models
{
    public class Role
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public Nullable<int> RoleParentId { get; set; }
        public bool RoleActive { get; set; }
        public System.DateTime RoleLastChange { get; set; }
        public Nullable<int> RoleLastChangeUserID { get; set; }
        public bool ISSYSTEM { get; set; }
        public Nullable<System.DateTime> LastChange { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }

        public List<Action> Actions { get; set; }
    }
}