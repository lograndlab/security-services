﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Logrand.LOS.Security.Auth.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        //public string UserPass { get; set; }
        //public string UserSalt { get; set; }
        public Nullable<bool> UserActive { get; set; }
        public Nullable<bool> UserBlocked { get; set; }
        public Nullable<byte> UserAttempts { get; set; }
        public Nullable<bool> UserCanChangePass { get; set; }
        public Nullable<DateTime> UserLastAccess { get; set; }
        public Nullable<DateTime> UserLastChange { get; set; }
        public int UserLastChangeUserId { get; set; }
        public Nullable<int> RoleId { get; set; }
        public Nullable<int> ContactId { get; set; }
        public Nullable<int> SalaId { get; set; }
        public string UserIpAccess { get; set; }
        public string PayrollNumber { get; set; }
        public DateTime UserCreatedDate { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> UserCreatedBy { get; set; }
        public string userDomain { get; set; }
        public Nullable<DateTime> LastChange { get; set; }
        public Nullable<DateTime> SyncDate { get; set; }
        public Person Person { get; set; }
        public List<Sala> Salas { get; set; }
        public List<Role> Roles { get; set; }
        public List<Action> Actions { get; set; }

        public static User validateUser(string UserName, string Password)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            var user = db.SEC_USER.Where(x => x.UserName == UserName).FirstOrDefault();
            
            if (user != null)
            {
                string passwordSalt = hashSHA1(Password + user.UserSalt);

                if(user.UserPass.Equals(passwordSalt))
                {
                    //User model
                    User model = new User();

                    model.UserId = user.UserId;
                    model.UserName = user.UserName;
                    //model.UserPass = user.UserPass;
                    //model.UserSalt = user.UserSalt;
                    model.UserActive = user.UserActive;
                    model.UserBlocked = user.UserBlocked;
                    model.UserAttempts = user.UserAttempts;
                    model.UserCanChangePass = user.UserCanChangePass;
                    model.UserLastAccess = user.UserLastAccess;
                    model.UserLastChange = user.UserLastChange;
                    model.UserLastChangeUserId = user.UserLastChangeUserId;
                    model.RoleId = user.RoleId;
                    model.ContactId = user.ContactId;
                    model.SalaId = user.SalaId;
                    model.UserIpAccess = user.UserIpAccess;
                    model.PayrollNumber = user.PayrollNumber;
                    model.UserCreatedDate = user.UserCreatedDate;
                    model.EmployeeId = user.EmployeeId;
                    model.UserCreatedBy = user.UserCreatedBy;
                    model.userDomain = user.userDomain;
                    model.LastChange = user.LastChange;
                    model.SyncDate = user.SyncDate;
                    model.Roles = getRoleBySecurityModelUser(user);
                    model.Salas = getSalasByUser(user);
                    model.Person = getPersonByEmployeeID((int)user.UserId);


                    user.UserLastAccess = DateTime.Now;
                    db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    return model;
                }
            }

            return null;
        }

        public static void addSessionLogin(string UserName, int ApplicationID, string token, DateTimeOffset? expires)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            var user = db.SEC_USER.Where(x => x.UserName == UserName).FirstOrDefault();

            //Poner en true para cerrar todas las sesiones anteriores, solo tendrá una sesion activa
            //if (true)
            //{
            //    var sessions = user.Sessions1.Where(x => x.Active == true && x.ApplicationID == ApplicationID);

            //    sessions.ToList().ForEach(x =>
            //    {
            //        x.LogoutAt = DateTime.Now;
            //        x.Active = false;
            //        db.Entry(x).State = System.Data.Entity.EntityState.Modified;
            //    });
            //} 

            Security.Models.SEC_SESSION session = new Security.Models.SEC_SESSION();

            session.UserID = user.UserId;
            session.ApplicationID = ApplicationID;
            session.AccessToken = token;
            session.IpAddress = "127.0.0.1";
            session.CreatedAt = DateTime.Now;
            session.ExpiredAt = expires.Value.LocalDateTime;
            session.Active = true;

            db.SEC_SESSION.Add(session);

            try
            {
                db.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                //Add log error
            }
        }

        public static List<Role> getRoleBySecurityModelUser(Security.Models.SEC_USER user)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();
            List<Role> roles = new List<Role>();
            foreach (Security.Models.SEC_ROLE_USER userRole in user.SEC_ROLE_USER)
            {
                Role role = new Role();

                role.RoleId = userRole.SEC_ROLE.RoleId;
                role.RoleName = userRole.SEC_ROLE.RoleName;
                role.RoleDescription = userRole.SEC_ROLE.RoleDescription;
                role.RoleParentId = userRole.SEC_ROLE.RoleParentId;
                role.RoleActive = userRole.SEC_ROLE.RoleActive;
                role.RoleLastChange = userRole.SEC_ROLE.RoleLastChange;
                role.RoleLastChangeUserID = userRole.SEC_ROLE.RoleLastChangeUserID;
                role.ISSYSTEM = userRole.SEC_ROLE.ISSYSTEM;
                role.LastChange = userRole.SEC_ROLE.LastChange;
                role.SyncDate = userRole.SEC_ROLE.RoleLastChange;

                var roleActions = db.SEC_PERMISSION_ROLE.Where(x=>x.RoleId == role.RoleId);

                List<Action> roleActionsModel = new List<Action>();

                foreach (Security.Models.SEC_PERMISSION_ROLE roleAction in roleActions)
                {
                    var permission = db.SEC_PERMISSION.Where(x => x.PermissionId == roleAction.PermissionId).FirstOrDefault();
                    if (permission != null)
                    {
                        Action action = new Action();

                        action.PermissionId = permission.PermissionId;
                        action.PermisionName = permission.PermisionName;
                        action.PermissionActive = permission.PermissionActive;
                        action.PermissionDescription = permission.PermissionDescription;
                        action.PermissionLastChange = permission.PermissionLastChange;
                        action.PermissionLastChangeUserId = permission.PermissionLastChangeUserId;
                        action.OpcMenuId = permission.OpcMenuId;
                        action.LastChange = permission.LastChange;
                        action.SyncDate = permission.SyncDate;

                        roleActionsModel.Add(action);
                    }
                }

                role.Actions = roleActionsModel;

                roles.Add(role);
            }

            return roles;
        }

        //public static List<Action> getActionsBySecurityModelUser(Security.Models.SEC_USER user)
        //{
        //    List<Action> actions = new List<Action>();
        //    foreach (Security.Models.UserAction userAction in user.UserActions)
        //    {
        //        Action action = new Action();

        //        action.ID = userAction.Action.ID;
        //        action.Name = userAction.Action.Name;
        //        action.Description = userAction.Action.Description;
        //        action.ModuleID = userAction.Action.ModuleID;
        //        action.CreatedAt = userAction.Action.CreatedAt;
        //        action.ModifiedAt = userAction.Action.ModifiedAt;
        //        action.Active = userAction.Action.Active;

        //        actions.Add(action);
        //    }

        //    return actions;
        //}

        private static List<Sala> getSalasByUser(Security.Models.SEC_USER user)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();
            var results = db.SEC_USER_SALA_GET(user.UserId, null, null);

            List<Sala> salas = new List<Sala>();

            foreach(Security.Models.SEC_USER_SALA_GET_Result item in results)
            {
                Sala sala = new Sala();

                sala.SalaId = item.SalaId;
                sala.SalaName = item.SalaName;
                sala.UserId = item.UserId;
                sala.UserName = item.UserName;

                salas.Add(sala);
            }

            return salas;
        }

        private static Person getPersonByEmployeeID(int employeeID)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();
            var result = db.SEC_PERSONBYEMPLOYEEID_GET(employeeID).FirstOrDefault();
            Person model = new Person();

            model.ID = result.ID;
            model.Name = result.Name;
            model.LastName = result.LastName;
            model.Birthday = result.Birthday;
            model.Gender = result.Gender;
            model.Email = result.Email;
            model.Phone = result.Phone;
            model.CURP = result.CURP;
            model.RFC = result.RFC;
            model.CreatedBy = result.CreatedBy;
            model.CreatedAt = result.CreatedAt;
            model.LastChange = result.LastChange;
            model.ModifiedBy = result.ModifiedBy;

            return model;
        }

        private static string hashSHA1(string input)
        {
            var hash = (new SHA1Managed()).ComputeHash(Encoding.UTF8.GetBytes(input));
            return string.Join("", hash.Select(b => b.ToString("x2")).ToArray()).ToUpper();
        }
    }
}