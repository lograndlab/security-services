﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Security.Auth.Models
{
    public class Application
    {
        public int AppId { get; set; }
        public string AppName { get; set; }
        public string AppImage { get; set; }
        public string AppURL { get; set; }
        public bool AppActive { get; set; }
        public string Domain { get; set; }
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public Nullable<System.DateTime> LastChange { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }

        public User User { get; set; }

        public static Application getApplicationByClientID(string ClientID)
        {
            Security.Models.SecurityEntities db = new Security.Models.SecurityEntities();

            var app = db.SEC_APPLICATION.Where(x => x.ClientID == ClientID).FirstOrDefault();

            if(app != null)
            {
                Application model = new Application();
                model.AppId = app.AppId;
                model.AppName = app.AppName;
                model.AppImage = app.AppImage;
                model.AppURL = app.AppURL;
                model.AppActive = app.AppActive;
                model.Domain = app.Domain;
                model.AppURL = app.AppURL;
                model.ClientID = app.ClientID;
                model.ClientSecret = app.ClientSecret;
                model.LastChange = app.LastChange;
                model.SyncDate = app.SyncDate;

                return model;
            }

            return null;
        }
    }
}