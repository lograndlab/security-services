﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Security.Auth.Models
{
    public class Action
    {
        public int PermissionId { get; set; }
        public string PermisionName { get; set; }
        public bool PermissionActive { get; set; }
        public string PermissionDescription { get; set; }
        public System.DateTime PermissionLastChange { get; set; }
        public int PermissionLastChangeUserId { get; set; }
        public int OpcMenuId { get; set; }
        public Nullable<System.DateTime> LastChange { get; set; }
        public Nullable<System.DateTime> SyncDate { get; set; }
    }
}