﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logrand.LOS.Security.Auth.Models
{
    public class Sala
    {
        public int SalaId { get; set; }
        public string SalaName { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
    }
}