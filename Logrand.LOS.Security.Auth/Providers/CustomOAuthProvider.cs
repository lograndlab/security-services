﻿using Logrand.LOS.Security.Models;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Logrand.LOS.Security.Auth.Providers
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId = string.Empty;
            string clientSecret = string.Empty;
            string symmetricKeyAsBase64 = string.Empty;

            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }

            if (context.ClientId == null)
            {
                context.SetError("invalid_client", "ClientId is not set");
                return Task.FromResult<object>(null);
            }

            var audience = Models.Application.getApplicationByClientID(context.ClientId);

            if (audience == null)
            {
                context.SetError("invalid_client", string.Format("Invalid client_id '{0}'", context.ClientId));
                return Task.FromResult<object>(null);
            }

            if (audience.AppActive == false)
            {
                context.SetError("inactive_client", string.Format("Inactive client_id '{0}'", context.ClientId));
                return Task.FromResult<object>(null);
            }

            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            Models.User user = Models.User.validateUser(context.UserName, context.Password); 

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect");
                return Task.FromResult<object>(null);
            }

            if (user.UserActive == false)
            {
                context.SetError("invalid_grant", "The user name is inactive");
                return Task.FromResult<object>(null);
            }

            var identity = new ClaimsIdentity("JWT");

            identity.AddClaim(new Claim("id", user.UserId.ToString()));
            identity.AddClaim(new Claim("username", user.UserName));
            identity.AddClaim(new Claim("name", user.Person.Name));
            identity.AddClaim(new Claim("lastname", user.Person.LastName));
            identity.AddClaim(new Claim("person", JsonConvert.SerializeObject(user.Person)));

            if (user.Roles.Count > 0)
            {
                foreach (Models.Role role in user.Roles)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, role.RoleId.ToString()));
                }
            }

            if(user.Salas.Count > 0)
            {
                foreach(Models.Sala sala in user.Salas)
                {
                    identity.AddClaim(new Claim("salas", JsonConvert.SerializeObject(new Models.Claim() { id = sala.SalaId, name = sala.SalaName })));
                }
            }

            var props = new AuthenticationProperties(new Dictionary<string, string>{
                { "id", user.UserId.ToString() },
                { "username", user.UserName },
                { "audience", (context.ClientId == null) ? string.Empty : context.ClientId },
                { "person", JsonConvert.SerializeObject(user.Person) },
                { "roles", JsonConvert.SerializeObject(user.Roles) },
                { "salas", JsonConvert.SerializeObject(user.Salas) },
                { "actions", JsonConvert.SerializeObject(user.Actions) },
            });

            var ticket = new AuthenticationTicket(identity, props);
            context.Validated(ticket);
            return Task.FromResult<object>(null);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }
}