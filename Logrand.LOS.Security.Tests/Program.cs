﻿using Microsoft.Owin.Security.DataHandler.Encoder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Logrand.LOS.Security.Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] appIDs = {
                1,
                2,
                3
            };
            foreach(int appID in appIDs)
            {
                var clientId = Guid.NewGuid().ToString("N");
                var key = new byte[64];
                RNGCryptoServiceProvider.Create().GetBytes(key);
                var base64Secret = TextEncodings.Base64Url.Encode(key);

                Console.WriteLine("UPDATE [Logrand_LOS_SEC].[dbo].[SEC_APPLICATION] SET [Domain] = '{0}', [ClientID] = '{1}', [ClientSecret] = '{2}' WHERE AppId = {3};", "http://localhost/security", clientId, base64Secret, appID);

            }

            Console.ReadKey();
        }
    }
}
